/**
 * @file sht3x.c
 * @brief Brief_description
 *
 * @date Apr 18, 2020
 * @author Ilias Daradimos
 */

#include "sht3x.h"

uint8_t SHT3x_CRC8(uint8_t *data, uint8_t len);
uint8_t SHT3x_CheckCRC(uint8_t *data, uint8_t len);
SHT3x_StatusTypeDef SHT3x_send_cmd(struct sht3x_dev *sensor, sht3x_command_t cmd);

#ifdef SHT3x_USE_WEAK_FUNCTIONS
__weak uint8_t SHT3x_i2c_read(uint8_t dev_id, uint16_t cmd, uint8_t *data,
		uint16_t len) {
__weak uint8_t SHT3x_i2c_write(uint8_t dev_id, uint16_t cmd);
	return 0;
}
#endif

SHT3x_StatusTypeDef SHT3x_init(struct sht3x_dev *sensor) {
	SHT3x_StatusTypeDef status;
	sensor->chip_reset();
	status = SHT3x_send_cmd(sensor, SHT3x_CMD_SOFT_RESET);
	switch (sensor->mode) {
		case SHT3x_CMD_MEAS_CLOCKSTR_H:
		case SHT3x_CMD_MEAS_CLOCKSTR_M:
		case SHT3x_CMD_MEAS_CLOCKSTR_L:
		case SHT3x_CMD_MEAS_POLLING_H:
		case SHT3x_CMD_MEAS_POLLING_M:
		case SHT3x_CMD_MEAS_POLLING_L:
			break;
		default:
			status = SHT3x_send_cmd(sensor, sensor->mode);
			break;
	}
	return status;
}

SHT3x_StatusTypeDef SHT3x_ReadData(struct sht3x_dev *sensor, uint16_t cmd,
		uint16_t *data, uint8_t len) {
	SHT3x_StatusTypeDef status = SHT_OK;
	uint8_t buf_len = len * 2 + len;
	uint8_t buf[buf_len], cntr, data_cntr = 0;
#ifdef SHT3x_USE_WEAK_FUNCTIONS
	status = SHT3x_i2c_read(sensor->address, cmd, buf, buf_len);
#else
	status = sensor->read(sensor->address, cmd, buf, buf_len);
#endif
	if (status != SHT_OK) return status;
	// Check CRC
	if (SHT3x_CheckCRC(buf, buf_len) != 0)
		return SHT_CHECKSUM_ERROR;
	// Extract Data
	for (cntr = 0; cntr < buf_len; cntr += 3) {
		data[data_cntr++] = buf[cntr + 1] | (buf[cntr] << 8);
	}
	return SHT_OK;
}

SHT3x_StatusTypeDef SHT3x_send_cmd(struct sht3x_dev *sensor, sht3x_command_t cmd) {
#ifdef SHT3x_USE_WEAK_FUNCTIONS
	return SHT3x_i2c_write(sensor->address, cmd);
#else
	return sensor->write(sensor->address, cmd);
#endif
}

SHT3x_StatusTypeDef SHT3x_ReadSerialNumber(struct sht3x_dev *sensor,
		uint32_t *serialNumber) {
	uint16_t buf[2];
	SHT3x_ReadData(sensor, SHT3x_CMD_READ_SERIALNBR, (uint16_t*) buf, 2);
	*serialNumber = buf[1] | (buf[0] << 16);
	return SHT_OK;
}

uint8_t SHT3x_CheckCRC(uint8_t *data, uint8_t len) {
	uint8_t cntr, status = 0;
	for (cntr = 0; cntr < len; cntr += 3) {
		status |= SHT3x_CRC8(data, 3);
	}
	return status;
}

uint8_t SHT3x_CRC8(uint8_t *data, uint8_t len) {
	uint8_t bit, cntr;
	uint8_t crc = 0xFF;

	for (cntr = 0; cntr < len; cntr++) {
		crc ^= (data[cntr]);
		for (bit = 8; bit > 0; --bit) {
			if (crc & 0x80)
				crc = (crc << 1) ^ SHT3x_POLYNOMIAL;
			else
				crc = (crc << 1);
		}
	}
	return crc;
}

SHT3x_StatusTypeDef SHT3x_clear_status_register(struct sht3x_dev *sensor) {
	return SHT3x_send_cmd(sensor, SHT3x_CMD_CLEAR_STATUS);
}

SHT3x_StatusTypeDef SHT3x_get_status(struct sht3x_dev *sensor) {
	return SHT3x_ReadData(sensor, SHT3x_CMD_READ_STATUS,
			(uint16_t*) &sensor->status_register, 1);
}

SHT3x_StatusTypeDef SHT3x_heater(struct sht3x_dev *sensor, _Bool enable) {
	if (enable)
		return SHT3x_send_cmd(sensor, SHT3x_CMD_HEATER_ENABLE);
	return SHT3x_send_cmd(sensor, SHT3x_CMD_HEATER_DISABLE);
}

SHT3x_StatusTypeDef SHT3x_get_ht(struct sht3x_dev *sensor, float *humidity,
		float *temperature) {
	uint16_t buf[2];
	SHT3x_StatusTypeDef status;
	switch (sensor->mode) {
			case SHT3x_CMD_MEAS_CLOCKSTR_H:
			case SHT3x_CMD_MEAS_CLOCKSTR_M:
			case SHT3x_CMD_MEAS_CLOCKSTR_L:
			case SHT3x_CMD_MEAS_POLLING_H:
			case SHT3x_CMD_MEAS_POLLING_M:
			case SHT3x_CMD_MEAS_POLLING_L:
				status = SHT3x_ReadData(sensor, sensor->mode, buf, 2);
				break;
			default:
				status = SHT3x_ReadData(sensor, SHT3x_CMD_FETCH_DATA, buf, 2);
				break;
		}
	*humidity = (buf[1] * 100.0 / 65535.0);
	*temperature = (buf[0] * 175.0 / 65535.0) - 45.0;
	return status;
}
