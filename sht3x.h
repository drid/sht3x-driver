/**
 * @file sht3x.h
 * @brief Brief_description
 *
 * @date Apr 18, 2020
 * @author Ilias Daradimos
 */

#ifndef SHT3X_H_
#define SHT3X_H_

#include <stdint.h>

#ifndef __weak
#define __weak __attribute__((weak))
#endif

#define SHT3x_POLYNOMIAL	0x31

// Sensor Commands
typedef enum {
	SHT3x_CMD_READ_SERIALNBR = 0x3780, // read serial number
	SHT3x_CMD_READ_STATUS = 0xF32D, // read status register
	SHT3x_CMD_CLEAR_STATUS = 0x3041, // clear status register
	SHT3x_CMD_HEATER_ENABLE = 0x306D, // enabled heater
	SHT3x_CMD_HEATER_DISABLE = 0x3066, // disable heater
	SHT3x_CMD_SOFT_RESET = 0x30A2, // soft reset
	SHT3x_CMD_FETCH_DATA = 0xE000, // readout measurements for periodic mode
	SHT3x_CMD_R_AL_LIM_LS = 0xE102, // read alert limits, low set
	SHT3x_CMD_R_AL_LIM_LC = 0xE109, // read alert limits, low clear
	SHT3x_CMD_R_AL_LIM_HS = 0xE11F, // read alert limits, high set
	SHT3x_CMD_R_AL_LIM_HC = 0xE114, // read alert limits, high clear
	SHT3x_CMD_W_AL_LIM_HS = 0x611D, // write alert limits, high set
	SHT3x_CMD_W_AL_LIM_HC = 0x6116, // write alert limits, high clear
	SHT3x_CMD_W_AL_LIM_LC = 0x610B, // write alert limits, low clear
	SHT3x_CMD_W_AL_LIM_LS = 0x6100, // write alert limits, low set
	SHT3x_CMD_NO_SLEEP = 0x303E,
} sht3x_command_t;

typedef enum {
	SHT3x_CMD_MEAS_CLOCKSTR_H = 0x2C06, // measurement: clock stretching, high repeatability
	SHT3x_CMD_MEAS_CLOCKSTR_M = 0x2C0D, // measurement: clock stretching, medium repeatability
	SHT3x_CMD_MEAS_CLOCKSTR_L = 0x2C10, // measurement: clock stretching, low repeatability
	SHT3x_CMD_MEAS_POLLING_H = 0x2400, // measurement: polling, high repeatability
	SHT3x_CMD_MEAS_POLLING_M = 0x240B, // measurement: polling, medium repeatability
	SHT3x_CMD_MEAS_POLLING_L = 0x2416, // measurement: polling, low repeatability
	SHT3x_CMD_MEAS_PERI_05_H = 0x2032, // measurement: periodic 0.5 mps, high repeatability
	SHT3x_CMD_MEAS_PERI_05_M = 0x2024, // measurement: periodic 0.5 mps, medium repeatability
	SHT3x_CMD_MEAS_PERI_05_L = 0x202F, // measurement: periodic 0.5 mps, low repeatability
	SHT3x_CMD_MEAS_PERI_1_H = 0x2130, // measurement: periodic 1 mps, high repeatability
	SHT3x_CMD_MEAS_PERI_1_M = 0x2126, // measurement: periodic 1 mps, medium repeatability
	SHT3x_CMD_MEAS_PERI_1_L = 0x212D, // measurement: periodic 1 mps, low repeatability
	SHT3x_CMD_MEAS_PERI_2_H = 0x2236, // measurement: periodic 2 mps, high repeatability
	SHT3x_CMD_MEAS_PERI_2_M = 0x2220, // measurement: periodic 2 mps, medium repeatability
	SHT3x_CMD_MEAS_PERI_2_L = 0x222B, // measurement: periodic 2 mps, low repeatability
	SHT3x_CMD_MEAS_PERI_4_H = 0x2334, // measurement: periodic 4 mps, high repeatability
	SHT3x_CMD_MEAS_PERI_4_M = 0x2322, // measurement: periodic 4 mps, medium repeatability
	SHT3x_CMD_MEAS_PERI_4_L = 0x2329, // measurement: periodic 4 mps, low repeatability
	SHT3x_CMD_MEAS_PERI_10_H = 0x2737, // measurement: periodic 10 mps, high repeatability
	SHT3x_CMD_MEAS_PERI_10_M = 0x2721, // measurement: periodic 10 mps, medium repeatability
	SHT3x_CMD_MEAS_PERI_10_L = 0x272A, // measurement: periodic 10 mps, low repeatability
} sht3x_mode_t;

typedef enum {
	SHT_OK = 0, // no error
	SHT_ERROR, // parameter out of range error
	SHT_CHECKSUM_ERROR, // checksum mismatch error
	SHT_TIMEOUT_ERROR // timeout error
} SHT3x_StatusTypeDef;

//-- Typedefs -----------------------------------------------------------------
// Status-Register
typedef union {
	uint16_t u16;
	struct {
#ifdef LITTLE_ENDIAN  // bit-order is little endian
    uint16_t CrcStatus     : 1; // write data checksum status
    uint16_t CmdStatus     : 1; // command status
    uint16_t Reserve0      : 2; // reserved
    uint16_t ResetDetected : 1; // system reset detected
    uint16_t Reserve1      : 5; // reserved
    uint16_t T_Alert       : 1; // temperature tracking alert
    uint16_t RH_Alert      : 1; // humidity tracking alert
    uint16_t Reserve2      : 1; // reserved
    uint16_t HeaterStatus  : 1; // heater status
    uint16_t Reserve3      : 1; // reserved
    uint16_t AlertPending  : 1; // alert pending status
    #else                 // bit-order is big endian
		uint16_t AlertPending :1;
		uint16_t Reserve3 :1;
		uint16_t HeaterStatus :1;
		uint16_t Reserve2 :1;
		uint16_t RH_Alert :1;
		uint16_t T_Alert :1;
		uint16_t Reserve1 :5;
		uint16_t ResetDetected :1;
		uint16_t Reserve0 :2;
		uint16_t CmdStatus :1;
		uint16_t CrcStatus :1;
#endif
	} bit;
} sht3x_status_register_t;

/*!
 * @brief Function pointer definitions
 */
typedef uint8_t (*sht3x_i2c_r_fptr_t)(uint8_t dev_id, uint16_t reg_addr,
		uint8_t *data, uint16_t len);
typedef uint8_t (*sht3x_i2c_w_fptr_t)(uint8_t dev_id, uint16_t cmd);
typedef void (*sht3x_gpio_fptr_t)();
typedef uint32_t (*sht3x_time_fptr_t)(void);
typedef void (*sht3x_delay_fptr_t)(uint32_t period);

/*!
 * @brief sht3x device structure
 */
struct sht3x_dev {
	uint8_t address;
	/*! Reset function pointer */
	sht3x_gpio_fptr_t chip_reset;
	/*! Read function pointer */
	sht3x_i2c_r_fptr_t read;
	/*! Write function pointer */
	sht3x_i2c_w_fptr_t write;
	/*! Get system tick function */
	sht3x_time_fptr_t get_tick;
	sht3x_status_register_t status_register;
	sht3x_mode_t mode;
};

SHT3x_StatusTypeDef SHT3x_ReadSerialNumber(struct sht3x_dev *sensor,
		uint32_t *serialNumber);
SHT3x_StatusTypeDef SHT3x_get_status(struct sht3x_dev *sensor);
SHT3x_StatusTypeDef SHT3x_init(struct sht3x_dev *sensor);
SHT3x_StatusTypeDef SHT3x_heater(struct sht3x_dev *sensor, _Bool enable);
SHT3x_StatusTypeDef SHT3x_get_ht(struct sht3x_dev *sensor, float *humidity,
		float *temperature);
#endif /* SHT3X_SHT3X_H_ */
